require "mock_api/configuration"
require "mock_api/requests"
require "mock_api/version"

module MockApi
  class << self

    # To set the configuration variables
    #
    # @return [Object]
    def configuration
      @configuration ||= Configuration.new
    end

    # Overwrites the default configuration to use the chosen one from the main app.
    #
    # @return [Void]
    def configure
      yield(configuration)
    end
  end
end
