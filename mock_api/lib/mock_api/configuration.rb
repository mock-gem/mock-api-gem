module MockApi
  class Configuration

    attr_accessor :project_name, :host

    # Default config values.
    #
    # It can be changed thorugh the main app in the config/initializers/mock_api.rb file.
    #
    # @return [String]
    def initialize
      @project_name = "DEFAULT_PROJECT_NAME"
      @host = "https://your_mock_server.com"
    end
  end
end
