require 'active_support/concern'
require 'uri'
require 'net/http'

module MockApi
  module Requests

    extend ActiveSupport::Concern

    included do

      # This method will handle the 404 error scenario.
      # If the real endpoint doesn’t exist and a Mock Endpoint wasn’t created for it, then the usual 404 error will be raised.
      #
      # @return [Json]
      def get_mock_response!
        mock_response = find_mock_request!

        return JSON.parse(mock_response.body), mock_response.code
      end

      # If someone makes a request to some endpoint that doesn’t exist yet, this method will redirect the request to the Mock Server.
      #
      # @return [Hash]
      def find_mock_request!
        host = MockApi.configuration.host
        url = request.original_fullpath
        request_type = request.original_request_method
        project_name = MockApi.configuration.project_name
        request_params = request.params.except(:controller, :action, :error).to_json
        uri = URI.parse("#{host}/mock_endpoints/search?url=#{url}&project=#{project_name}&request_type=#{request_type}&request_params=#{request_params}")
        Net::HTTP.get_response(uri)
      end
    end
  end
end
