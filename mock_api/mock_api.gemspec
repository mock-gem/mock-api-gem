
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "mock_api/version"

Gem::Specification.new do |spec|
  spec.name          = "mock_api"
  spec.version       = MockApi::VERSION
  spec.authors       = ["Diego M."]
  spec.email         = ["diego.m@allerin.nl"]

  spec.summary       = %q{Create Mock Endpoints for your application.}
  spec.description   = %q{The main goal of this Gem is to be able to create Mock Endpoints so if your team work is divided in FE and BE team, FE team can start working on their tasks even if the real endpoint has not been created yet.}
  spec.homepage      = "https://ars-api.allerintech.com"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'aasm', "~> 5.2"
  spec.add_runtime_dependency 'activemodel'
  spec.add_runtime_dependency 'active_interaction'
  spec.add_runtime_dependency 'activerecord'

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
