# MockApi

MockApi is a gem to handle sample requests in a Mock Server.   

If the route doesn't exist yet, MockApi will look for a sample response of that endpoint in our Mock Server.

This will remove the dependency between FE and BE teams if the API is not finished, approved or merged yet.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'mock_api'
```

And then execute:

    $ bundle install


## Usage

Before start using MockApi, you need to do some steps:

Add a configuration file in `config/initializers/mock_api.rb` and set your project name:

```ruby
MockApi.configure do |config|
  config.project_name = 'PROJECT' #Replace PROJECT with your actual project name.
  config.host = 'https://some_host_here.com'
end
```

Add this line in your `config/application.rb` file to avoid the `ActionController::RoutingError` from being raised if the endpoint doesn't exist:

```ruby
config.exceptions_app = self.routes
```

We need to add a new file inside of the `config/initializers` folder to have a new method that allow us to keep the original request method, otherwise, middleware will replace our method with a get request. So, add a `config/initializers/action_dispatch.rb` file and add this code:

```ruby
module ActionDispatch
  class Request

    # Middleware overwrites the original request method to replace it for a GET Method.
    # This method patch is to avoid missing that information.
    #
    # @return [String]
    def original_request_method
      return env['action_dispatch.original_request_method']
    end
  end
end
```

Now create a new controller and add some action to handle your 404 requests. Example, create an `Errors` controller with a `not_found` method:

    $ rails g controller Errors not_found

Include the `MockApi::Requests` module to your new controller:

```ruby
class ErrorsController < ActionController::Base
  include MockApi::Requests

  def not_found
  end
end
```

Add a route pointing to your new action/controller in `config/routes.rb`:

```ruby
match '/404', to: 'errors#not_found', via: :all
```

Now just get the mock response with the `get_mock_response!` method and handle the result as you want!

**Notes:**

1. `get_mock_response!` will return two values: the response body and the response status code. Please keep that in mind before handling the final result.

2. Please keep in mind that if the endpoint is not created in our Mock Server, MockApi will return the usual 404 error.

Example of use:

```ruby
class ErrorsController < ActionController::Base
  include MockApi::Requests

  def not_found
    body, status = get_mock_response!
    render json: body, status: status
  end
end
```

# Mock Server

The Mock Server can be found at **Link here**. Add all the endpoints you need there along with each necessary atribute to make it work (url, request method, project name, response, etc.)  
